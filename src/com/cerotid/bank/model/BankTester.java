package com.cerotid.bank.model;

import java.util.ArrayList;

import com.cerotid.bo.BankBOImpl;

public class BankTester {
	public static void main(String[] args) {
	
		// Address1
		Address add1 = new Address("123 Linda Lane", "Mansfield", "OH", "76221");

		//Address2
		Address add2 = new Address("115 Waterhose Dr",  "Arlington", "TX", "76010");

		// Account of customer 1
		Account c1acc1 = new Account();
		Account c1acc2 = new Account();
		Account c1acc3 = new Account();
		
		
		c1acc1.setAccountType(AccountType.BUSINESS_CHECKING);
		c1acc1.setAmount(100000.0);
		
		c1acc2.setAccountType(AccountType.CHECKING);
		c1acc3.setAccountType(AccountType.SAVINGS);

		//Account of customer 2
		Account c2acc1 = new Account();
		Account c2acc2 = new Account();
		
		c2acc1.setAccountType(AccountType.CHECKING);
		c2acc2.setAccountType(AccountType.SAVINGS);
		

		// ArrayList of customer 1's accounts
		ArrayList<Account> c1acc = new ArrayList<>();
		c1acc.add(c1acc1);
		c1acc.add(c1acc2);
		c1acc.add(c1acc3);
		
		// ArrayList of customer 2's accounts
		ArrayList<Account> c2acc = new ArrayList<>();
		c2acc.add(c2acc1);
		c2acc.add(c2acc2);
		
		  
		// Customer 1
		Customer c1 = new Customer();
		c1.setFirstName("Ashish");
		c1.setLastName("Pandey");
		c1.setAddress(add1);
		c1.setAccounts(c1acc);
		c1.setSsn("123-123-1233");
		
		//Customer 2
		Customer c2 = new Customer();
		c2.setFirstName("Pratiksha");
		c2.setLastName("Pandey Giri");
		c2.setAddress(add2);
		c2.setAccounts(c2acc);
		
		//
		Customer c3 = new Customer();
		c3.setFirstName("Aryan");
		c3.setLastName("Lupi");
		
		//Customers
		ArrayList<Customer> customers = new ArrayList<>();
		customers.add(c1);
		customers.add(c2);
		
		// Bank
		Bank b1 = new Bank();
		b1.setCustomers(customers);

		
//		b1.printBankName();
//		b1.printBankDetails();
//		c1.printCustomerAccounts();
//		
		Transaction t1 = new Transaction();
		BankBOImpl a = new BankBOImpl();
		
		
		a.addCustomer(b1, c3);
		
		Account ac = new Account();
		ac.setAccountType(AccountType.CHECKING);
		ac.setAmount(20000.0);
		
		ac.printAccountInfo();
		
		
		a.openAccount(c3, c1acc2);		
		
		
		System.out.println();
		System.out.println();
		b1.printBankDetails();		
	
		ac.printAccountInfo();
		
		System.out.println();
		
		try {
			ArrayList<Customer> myCustomers = a.getcustomerByState(b1, "tx");
	
			for (Customer c: myCustomers) {
				System.out.println(c.getFirstName() +" "+ c.getLastName() );
			}
			
		}catch (Exception e) {
			System.out.println("No such entry found");
		}
	//	a.printBankStatus(b1);
		System.out.println();
		System.out.println();
		a.depositMoneyInCustomerAccount(c1);
		
		try {
			Customer c = a.getCustomerInfo(b1, "123-123-1233");
			System.out.println("Name: "+c.getFirstName()+ " "+c.getLastName());
			System.out.println("Accounts: "+ c.getAccounts());
			
		}catch (NullPointerException e) {
			System.out.println("Sorry that SSN does not exist in the system");
		}
		
		
	}

}
