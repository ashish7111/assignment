package com.cerotid.bank.model;

public class WireTransfer extends Transaction {

	private String beneficiaryfirstname;
	private String beneficiaryLastName;
	private String intermediaryBankSWIFTCode;
	private String beneficiaryBankName;
	private String benficiaryAccountNumber;

	public String getBeneficiaryfirstname() {
		return beneficiaryfirstname;
	}

	public void setBeneficiaryfirstname(String beneficiaryfirstname) {
		this.beneficiaryfirstname = beneficiaryfirstname;
	}

	public String getBeneficiaryLastName() {
		return beneficiaryLastName;
	}

	public void setBeneficiaryLastName(String beneficiaryLastName) {
		this.beneficiaryLastName = beneficiaryLastName;
	}

	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}

	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBenficiaryAccountNumber() {
		return benficiaryAccountNumber;
	}

	public void setBenficiaryAccountNumber(String benficiaryAccountNumber) {
		this.benficiaryAccountNumber = benficiaryAccountNumber;
	}

}
