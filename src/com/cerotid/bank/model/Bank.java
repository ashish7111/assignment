package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Bank implements Serializable {
	// TODO
	private final String bankName = "AB Bank";
	private ArrayList<Customer> customers;

	public String getBankName() {
		return bankName;
	}



	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public void printBankName() {
		System.out.println(bankName);
		
	}

	public void printBankDetails() {

		System.out.println(toString());
		

	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	

}
