package com.cerotid.bank.model;

public class Transaction {
	
	private String receiverFirstName, receiverLastName;
	private double amount, fee;

	
	
	//Unloaded Constructor
	public Transaction() {
	
	}
	
	//Overloaded Constructor

	public Transaction(String receiverFirstName, String receiverLastName, double amount, double fee) {
		super();
		this.receiverFirstName = receiverFirstName;
		this.receiverLastName = receiverLastName;
		this.amount = amount;
		this.fee = fee;
	}
	
	//Setters and Getters
	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void createTransaction() {
		Customer c1 = new Customer();
		System.out.println("Transaction Created for "+ c1.getFirstName()+" "+c1.getLastName());
	}

	//Deducts the balance from the Existing balance
	
	public void deductAccountBalance(double bal) {
		bal = bal-amount-fee;
		System.out.println(bal);
	}
	
	

}
