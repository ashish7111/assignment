package com.cerotid.bank.model;

public enum AccountType {

	CHECKING("Checking"), SAVINGS("Savings"), BUSINESS_CHECKING("Business Checking");

	// Adding a string value to the enum constants
	private String accType;

	AccountType(String accType) {
		this.accType = accType;
	}

	public String getAccType() {
		return accType;
	}

}
