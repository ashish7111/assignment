package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Account {

	//Field in the Account Class
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amount;

	
	//Getters and setters to access the private fields 
	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	// Method to print the information of the account
	public void printAccountInfo() {
		System.out.println(toString());

	}


	//Overridden toString() to display the records in this Class
	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amount=" + amount + "]";
	}

}
