package com.cerotid.bo;

import java.util.ArrayList;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public interface BankBO {
	public void addCustomer(Bank bank, Customer customer);	
	public void openAccount(Customer customer, Account account);
	public ArrayList<Customer> getcustomerByState (Bank bank, String StateCode);
	public void printBankStatus(Bank bank);
	public void depositMoneyInCustomerAccount(Customer customer);
	public void sendMoney(Customer customer, Transaction transaction);
	public Customer getCustomerInfo(Bank bank, String ssn);
	

}
