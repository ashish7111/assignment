package com.cerotid.bo;

import java.util.ArrayList;
import java.util.Scanner;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public class BankBOImpl implements BankBO {

	//Adds a customer
	@Override
	public void addCustomer(Bank bank, Customer customer) {
		if (bank.getCustomers() != null) {
			bank.getCustomers().add(customer);
		} else {
			ArrayList<Customer> cList = new ArrayList<>();
			cList.add(customer);
			bank.setCustomers(cList);
		}

	}

	//Opens a new account
	@Override
	public void openAccount(Customer customer, Account account) {
		if (customer.getAccounts() != null) {

			ArrayList<Account> accList = customer.getAccounts();
			for (Account a : accList) {
				if (a.getAccountType() == account.getAccountType()) {
					System.out.println("Sorry the customer already has that account.");
					break;
				} else {
					customer.getAccounts().add(account);
				}
			}
		} else {
			ArrayList<Account> accList = new ArrayList<>();
			accList.add(account);
			customer.setAccounts(accList);
		}
	}

	// Gets Customer based on the state code provided
	@Override
	public ArrayList<Customer> getcustomerByState(Bank bank, String StateCode) {
		if (bank.getCustomers() != null) {
			ArrayList<Customer> custList = bank.getCustomers();
			ArrayList<Customer> returnList = new ArrayList<>();
			for (Customer c : custList) {
				Address add = c.getAddress();
				// Case Equals with ignore case validation
				if (add.getStateCode().equalsIgnoreCase(StateCode)) {
					returnList.add(c);
					return returnList;
				}
			}
		} else
			System.out.println("No customers in the bank");
		return null;
	}

	// Bank details
	@Override
	public void printBankStatus(Bank bank) {
		bank.printBankDetails();
	}

	
	//deposits money into customers account
	// fix logic
	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		if (customer.getAccounts() != null) {
			ArrayList<Account> custList = customer.getAccounts();
			Account acc = new Account();
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the amount you would like deposit");
			double amount = sc.nextDouble();

			System.out.println("Which account do you want to use?");
			System.out.println("\nEnter: 1 for Checking\nEnter: 2 for Savings" + "\nEnter : 3 for Business Checking");
			int accType = sc.nextInt();

			switch (accType) {
			case 1:
				for (Account ac : custList) {
					if (ac.getAccountType().equals(AccountType.CHECKING)) {
						double newAmount = ac.getAmount() + amount;
						System.out.println("Updated amount :" + newAmount);
					} else
						System.out.println("No account present");
				} break;
			case 2:
				for (Account ac : custList) {
					if (ac.getAccountType().equals(AccountType.SAVINGS)) {
						double newAmount = ac.getAmount() + amount;
						System.out.println("Updates amount :" + newAmount);
					} else
						System.out.println("No acc present");
				}break;
			case 3:
				for (Account ac : custList) {
					if (ac.getAccountType().equals(AccountType.BUSINESS_CHECKING)) {
						double newAmount = ac.getAmount() + amount;
						System.out.println("Updates amount :" + newAmount);
					} else
						System.out.println("No account present");
				}break;
			}

		} else
			System.out.println("No account present for the customer");
	}

	//Get info based on SSN 
	@Override
	public Customer getCustomerInfo(Bank bank, String ssn) {
		ArrayList<Customer> custList = bank.getCustomers();
		Customer cust = new Customer();
		for (Customer c : custList) {
			if (c.getSsn() != null) {
				if (c.getSsn().equalsIgnoreCase(ssn)) {
					cust = c;
				}else cust = null;
			}			
		}
		return cust;
	}

	// Not Complete
	// fix logic
	@Override
	public void sendMoney(Customer customer, Transaction transaction) {
		if (customer.getAccounts() != null) {
			ArrayList<Account> custList = customer.getAccounts();
			System.out.println("Which account do you want to use?");
			Scanner sc = new Scanner(System.in);
			System.out.println("\nEnter: 1 for Checking\nEnter: 2 for Savings" + "\nEnter : 3 for Business Checking");
			int i = sc.nextInt();
			Account acc = new Account();

			switch (i) {
			case 1:
				for (Account ac : custList) {
					if (acc.getAccountType().equals(AccountType.CHECKING)) {
						acc.getAmount();
					}
				}break;
			case 2:
				for (Account ac : custList) {
					if (acc.getAccountType().equals(AccountType.SAVINGS)) {
						acc.getAmount();
					}
				}break;
			case 3:
				for (Account ac : custList) {
					if (acc.getAccountType().equals(AccountType.BUSINESS_CHECKING)) {
						acc.getAmount();
					}
				}break;
			}

		}
	}

}
